# If any of the following macros should be set otherwise,
# you can wrap any of them with the following conditions:
# - %%if 0%%{centos} == 7
# - %%if 0%%{?rhel} == 7
# - %%if 0%%{?fedora} == 23
# Or just test for particular distribution:
# - %%if 0%%{centos}
# - %%if 0%%{?rhel}
# - %%if 0%%{?fedora}
#
# Be aware, on centos, both %%rhel and %%centos are set. If you want to test
# rhel specific macros, you can use %%if 0%%{?rhel} && 0%%{?centos} == 0 condition.
# (Don't forget to replace double percentage symbol with single one in order to apply a condition)

# Generate devel rpm
%global with_devel 1
# Build project from bundled dependencies
%global with_bundled 0
# Build with debug info rpm
%global with_debug 1
# Run tests in check section
%global with_check 1
# Generate unit-test rpm
%global with_unit_test 1

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%if ! 0%{?gobuild:1}
%define gobuild(o:) go build -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n')" -a -v -x %{?**};
%endif

%global provider        github
%global provider_tld    com
%global project         hashicorp
%global repo            vault
# https://github.com/hashicorp/vault
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global commit          9617c6eb79c3453aa569d620fc71e90f7e32f72f
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

Name:           %{repo}
Version:        0.5.3
Release:        1%{?dist}
Summary:        A tool for managing secrets
# Detected licences
# - *No copyright* MPL (v2.0) at 'LICENSE'
License:        MPLv2.0
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz
Source1:        vault.service
Source2:        vault.sysconfig
Source3:        vault-config.hcl
Source4:        vault-keys.txt

# Needed because the version of hashicorp/golang-lru in Fedora is too old
Patch0:         vault-0.5.3-twoqueue.patch

# Needed because the version of hashicorp/go-cleanhttp in Fedora is too old
Patch1:         vault-0.5.3-defaulttransport.patch

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 aarch64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}

# Need _unitdir macro
BuildRequires:  systemd-units

%if ! 0%{?with_bundled}
# Remaining dependencies not included in main packages
BuildRequires: golang(github.com/hashicorp/hcl)
BuildRequires: golang(github.com/hashicorp/go-uuid)
BuildRequires: golang(github.com/coreos/etcd/client)
BuildRequires: golang(github.com/denisenkom/go-mssqldb)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/ec2metadata)
BuildRequires: golang(github.com/ghodss/yaml)
BuildRequires: golang(golang.org/x/crypto/bcrypt)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)
BuildRequires: golang(github.com/hashicorp/go-multierror)
BuildRequires: golang(golang.org/x/crypto/ssh/agent)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/dynamodb)
#BuildRequires: golang(github.com/mitchellh/cli)
BuildRequires: golang-github-mitchellh-cli-devel-temporary
BuildRequires: golang(github.com/hashicorp/logutils)
BuildRequires: golang(github.com/hashicorp/errwrap)
BuildRequires: golang(golang.org/x/sys/unix)
BuildRequires: golang(github.com/mitchellh/mapstructure)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/iam)
BuildRequires: golang(github.com/armon/go-metrics)
BuildRequires: golang(golang.org/x/crypto/ssh/terminal)
BuildRequires: golang(github.com/mitchellh/go-homedir)
BuildRequires: golang(github.com/samuel/go-zookeeper/zk)
BuildRequires: golang(golang.org/x/crypto/ssh)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/awserr)
BuildRequires: golang(github.com/duosecurity/duo_api_golang/authapi)
BuildRequires: golang(github.com/google/go-github/github)
BuildRequires: golang(github.com/gocql/gocql)
BuildRequires: golang(github.com/ryanuber/columnize)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/s3)
BuildRequires: golang(golang.org/x/oauth2)
BuildRequires: golang(github.com/lib/pq)
BuildRequires: golang(github.com/hashicorp/go-cleanhttp)
BuildRequires: golang(golang.org/x/crypto/openpgp/packet)
BuildRequires: golang(golang.org/x/net/context)
BuildRequires: golang(github.com/fatih/structs)
BuildRequires: golang(github.com/hashicorp/golang-lru)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/sts)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/session)
BuildRequires: golang(github.com/go-sql-driver/mysql)
BuildRequires: golang(github.com/asaskevich/govalidator)
BuildRequires: golang(github.com/mitchellh/copystructure)
BuildRequires: golang(github.com/hashicorp/consul/api)
BuildRequires: golang(github.com/hashicorp/hcl/hcl/ast)
BuildRequires: golang(github.com/armon/go-radix)
BuildRequires: golang(github.com/duosecurity/duo_api_golang)
BuildRequires: golang(github.com/mitchellh/reflectwalk)
BuildRequires: golang(github.com/hashicorp/go-syslog)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute)
BuildRequires: golang(golang.org/x/crypto/openpgp)
BuildRequires: golang(github.com/go-ldap/ldap)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws)
BuildRequires: golang(github.com/coreos/etcd/pkg/transport)
%endif

%description
Vault is a tool for securely accessing secrets. A secret is anything that you
want to tightly control access to, such as API keys, passwords, certificates,
and more. Vault provides a unified interface to any secret, while providing
tight access control and recording a detailed audit log.

%if 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-devel
Summary:       %{summary}
BuildArch:     noarch

%if 0%{?with_check} && ! 0%{?with_bundled}
BuildRequires: golang(github.com/armon/go-metrics)
BuildRequires: golang(github.com/armon/go-radix)
BuildRequires: golang(github.com/asaskevich/govalidator)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/awserr)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/ec2metadata)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/session)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/dynamodb)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/iam)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/s3)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/sts)
BuildRequires: golang(github.com/coreos/etcd/client)
BuildRequires: golang(github.com/coreos/etcd/pkg/transport)
BuildRequires: golang(github.com/denisenkom/go-mssqldb)
BuildRequires: golang(github.com/duosecurity/duo_api_golang)
BuildRequires: golang(github.com/duosecurity/duo_api_golang/authapi)
BuildRequires: golang(github.com/fatih/structs)
BuildRequires: golang(github.com/ghodss/yaml)
BuildRequires: golang(github.com/go-ldap/ldap)
BuildRequires: golang(github.com/go-sql-driver/mysql)
BuildRequires: golang(github.com/gocql/gocql)
BuildRequires: golang(github.com/google/go-github/github)
BuildRequires: golang(github.com/hashicorp/consul/api)
BuildRequires: golang(github.com/hashicorp/errwrap)
BuildRequires: golang(github.com/hashicorp/go-cleanhttp)
BuildRequires: golang(github.com/hashicorp/go-multierror)
BuildRequires: golang(github.com/hashicorp/go-syslog)
BuildRequires: golang(github.com/hashicorp/go-uuid)
BuildRequires: golang(github.com/hashicorp/golang-lru)
BuildRequires: golang(github.com/hashicorp/hcl)
BuildRequires: golang(github.com/hashicorp/hcl/hcl/ast)
BuildRequires: golang(github.com/hashicorp/logutils)
BuildRequires: golang(github.com/lib/pq)
#BuildRequires: golang(github.com/mitchellh/cli)
BuildRequires: golang-github-mitchellh-cli-devel-temporary
BuildRequires: golang(github.com/mitchellh/copystructure)
BuildRequires: golang(github.com/mitchellh/go-homedir)
BuildRequires: golang(github.com/mitchellh/mapstructure)
BuildRequires: golang(github.com/mitchellh/reflectwalk)
BuildRequires: golang(github.com/ryanuber/columnize)
BuildRequires: golang(github.com/samuel/go-zookeeper/zk)
BuildRequires: golang(golang.org/x/crypto/bcrypt)
BuildRequires: golang(golang.org/x/crypto/openpgp)
BuildRequires: golang(golang.org/x/crypto/openpgp/packet)
BuildRequires: golang(golang.org/x/crypto/ssh)
BuildRequires: golang(golang.org/x/crypto/ssh/agent)
BuildRequires: golang(golang.org/x/crypto/ssh/terminal)
BuildRequires: golang(golang.org/x/net/context)
BuildRequires: golang(golang.org/x/oauth2)
BuildRequires: golang(golang.org/x/sys/unix)
%endif

Requires:      golang(github.com/armon/go-metrics)
Requires:      golang(github.com/armon/go-radix)
Requires:      golang(github.com/asaskevich/govalidator)
Requires:      golang(github.com/aws/aws-sdk-go/aws)
Requires:      golang(github.com/aws/aws-sdk-go/aws/awserr)
Requires:      golang(github.com/aws/aws-sdk-go/aws/credentials)
Requires:      golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)
Requires:      golang(github.com/aws/aws-sdk-go/aws/ec2metadata)
Requires:      golang(github.com/aws/aws-sdk-go/aws/session)
Requires:      golang(github.com/aws/aws-sdk-go/service/dynamodb)
Requires:      golang(github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute)
Requires:      golang(github.com/aws/aws-sdk-go/service/iam)
Requires:      golang(github.com/aws/aws-sdk-go/service/s3)
Requires:      golang(github.com/aws/aws-sdk-go/service/sts)
Requires:      golang(github.com/coreos/etcd/client)
Requires:      golang(github.com/coreos/etcd/pkg/transport)
Requires:      golang(github.com/denisenkom/go-mssqldb)
Requires:      golang(github.com/duosecurity/duo_api_golang)
Requires:      golang(github.com/duosecurity/duo_api_golang/authapi)
Requires:      golang(github.com/fatih/structs)
Requires:      golang(github.com/ghodss/yaml)
Requires:      golang(github.com/go-ldap/ldap)
Requires:      golang(github.com/go-sql-driver/mysql)
Requires:      golang(github.com/gocql/gocql)
Requires:      golang(github.com/google/go-github/github)
Requires:      golang(github.com/hashicorp/consul/api)
Requires:      golang(github.com/hashicorp/errwrap)
Requires:      golang(github.com/hashicorp/go-cleanhttp)
Requires:      golang(github.com/hashicorp/go-multierror)
Requires:      golang(github.com/hashicorp/go-syslog)
Requires:      golang(github.com/hashicorp/go-uuid)
Requires:      golang(github.com/hashicorp/golang-lru)
Requires:      golang(github.com/hashicorp/hcl)
Requires:      golang(github.com/hashicorp/hcl/hcl/ast)
Requires:      golang(github.com/hashicorp/logutils)
Requires:      golang(github.com/lib/pq)
#Requires:      golang(github.com/mitchellh/cli)
Requires:      golang-github-mitchellh-cli-devel-temporary
Requires:      golang(github.com/mitchellh/copystructure)
Requires:      golang(github.com/mitchellh/go-homedir)
Requires:      golang(github.com/mitchellh/mapstructure)
Requires:      golang(github.com/mitchellh/reflectwalk)
Requires:      golang(github.com/ryanuber/columnize)
Requires:      golang(github.com/samuel/go-zookeeper/zk)
Requires:      golang(golang.org/x/crypto/bcrypt)
Requires:      golang(golang.org/x/crypto/openpgp)
Requires:      golang(golang.org/x/crypto/openpgp/packet)
Requires:      golang(golang.org/x/crypto/ssh)
Requires:      golang(golang.org/x/crypto/ssh/agent)
Requires:      golang(golang.org/x/crypto/ssh/terminal)
Requires:      golang(golang.org/x/net/context)
Requires:      golang(golang.org/x/oauth2)
Requires:      golang(golang.org/x/sys/unix)

Provides:      golang(%{import_path}/api) = %{version}-%{release}
Provides:      golang(%{import_path}/audit) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/audit/file) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/audit/syslog) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/credential/app-id) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/credential/cert) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/credential/github) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/credential/ldap) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/credential/userpass) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/aws) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/cassandra) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/consul) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/mssql) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/mysql) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/pki) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/postgresql) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/ssh) = %{version}-%{release}
Provides:      golang(%{import_path}/builtin/logical/transit) = %{version}-%{release}
Provides:      golang(%{import_path}/cli) = %{version}-%{release}
Provides:      golang(%{import_path}/command) = %{version}-%{release}
Provides:      golang(%{import_path}/command/server) = %{version}-%{release}
Provides:      golang(%{import_path}/command/token) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/certutil) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/flag-kv) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/flag-slice) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/gated-writer) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/kdf) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/kv-builder) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/mfa) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/mfa/duo) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/mlock) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/password) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/pgpkeys) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/salt) = %{version}-%{release}
Provides:      golang(%{import_path}/helper/xor) = %{version}-%{release}
Provides:      golang(%{import_path}/http) = %{version}-%{release}
Provides:      golang(%{import_path}/logical) = %{version}-%{release}
Provides:      golang(%{import_path}/logical/framework) = %{version}-%{release}
Provides:      golang(%{import_path}/logical/testing) = %{version}-%{release}
Provides:      golang(%{import_path}/physical) = %{version}-%{release}
Provides:      golang(%{import_path}/shamir) = %{version}-%{release}
Provides:      golang(%{import_path}/vault) = %{version}-%{release}
Provides:      golang(%{import_path}/version) = %{version}-%{release}

%description -n golang-%{provider}-%{project}-%{repo}-devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
Summary:         Unit tests for %{name} package
%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        %{name}-devel = %{version}-%{release}

%if 0%{?with_check} && ! 0%{?with_bundled}
BuildRequires: golang(github.com/aws/aws-sdk-go/service/ec2)
%endif

Requires:      golang(github.com/aws/aws-sdk-go/service/ec2)

%description -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
%{summary}

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif

%prep
%setup -q -n %{repo}-%{commit}
%patch0 -p1
%patch1 -p1

%build
mkdir -p src/%{provider}.%{provider_tld}/%{project}
ln -s ../../../ src/%{import_path}

%if ! 0%{?with_bundled}
export GOPATH=$(pwd):%{gopath}
%else
# No dependency directories so far
export GOPATH=$(pwd):%{gopath}
%endif

%gobuild -o bin/%{name} %{import_path}

%install
install -d -p %{buildroot}%{_bindir}
install -p -m 0755 bin/%{name} %{buildroot}%{_bindir}

# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . \( -iname "*.go" -or -iname "*.s" \) \! -iname "*_test.go" | grep -v "vendor") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test-devel.file-list
for file in $(find . -iname "*_test.go" -o -wholename "*/test-fixtures/*" | grep -v "vendor") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

install -D -p -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service
install -D -p -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -D -p -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/%{name}.d/default.hcl
install -D -p -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/%{name}.d/keys.txt
install -d -p %{buildroot}%{_sharedstatedir}/%{name}

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else
# Since we aren't packaging up the vendor directory we need to link
# back to it somehow. Hack it up so that we can add the vendor
# directory from BUILD dir as a gopath to be searched when executing
# tests from the BUILDROOT dir.
ln -s ./ ./vendor/src # ./vendor/src -> ./vendor

export GOPATH=%{buildroot}/%{gopath}:$(pwd)/vendor:%{gopath}
%endif

%if ! 0%{?gotest:1}
%global gotest go test
%endif

%gotest %{import_path}
%gotest %{import_path}/api
%gotest %{import_path}/audit
%gotest %{import_path}/builtin/credential/app-id
%gotest %{import_path}/builtin/credential/cert
%gotest %{import_path}/builtin/credential/github
%gotest %{import_path}/builtin/credential/ldap
%gotest %{import_path}/builtin/credential/userpass
%gotest %{import_path}/builtin/logical/aws
%gotest %{import_path}/builtin/logical/cassandra
%gotest %{import_path}/builtin/logical/consul
%gotest %{import_path}/builtin/logical/mssql
%gotest %{import_path}/builtin/logical/mysql
%gotest %{import_path}/builtin/logical/pki
%gotest %{import_path}/builtin/logical/postgresql
%gotest %{import_path}/builtin/logical/ssh
%gotest %{import_path}/builtin/logical/transit
%gotest %{import_path}/command
%gotest %{import_path}/command/server
%gotest %{import_path}/command/token
%gotest %{import_path}/helper/certutil
%gotest %{import_path}/helper/flag-kv
%gotest %{import_path}/helper/flag-slice
%gotest %{import_path}/helper/gated-writer
%gotest %{import_path}/helper/kdf
%gotest %{import_path}/helper/kv-builder
%gotest %{import_path}/helper/mfa
%gotest %{import_path}/helper/mfa/duo
# pgpkeys tests try to talk to keybase.io
# %gotest %{import_path}/helper/pgpkeys
%gotest %{import_path}/helper/salt
%gotest %{import_path}/helper/xor
%gotest %{import_path}/http
%gotest %{import_path}/logical
%gotest %{import_path}/logical/framework
%gotest %{import_path}/logical/testing
%gotest %{import_path}/physical
%gotest %{import_path}/shamir
%gotest %{import_path}/vault
%endif

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} \
    -d %{_sharedstatedir}/%{name} -s /sbin/nologin -c "vault user" %{name}

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc CHANGELOG.md README.md CONTRIBUTING.md
%dir %attr(0750, -, vault) %{_sysconfdir}/%{name}.d
%dir %attr(0750, %{name}, %{name}) %{_sharedstatedir}/%{name}
%config(noreplace) %attr(0640, -, vault) %{_sysconfdir}/%{name}.d/default.hcl
%config(noreplace) %attr(0640, -, vault) %{_sysconfdir}/%{name}.d/keys.txt
%config(noreplace) %attr(0640, -, vault) %{_sysconfdir}/sysconfig/%{name}
%attr(0755, -, -) %caps(cap_ipc_lock=+pe) %{_bindir}/%{name}
%{_unitdir}/%{name}.service

%if 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-devel -f devel.file-list
%license LICENSE
%doc CHANGELOG.md README.md CONTRIBUTING.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-unit-test-devel -f unit-test-devel.file-list
%license LICENSE
%doc CHANGELOG.md README.md CONTRIBUTING.md
%endif

%changelog
* Sun Mar 05 2017 Ed Marshall <esm@logic.net> 0.5.3-1
- First package for Fedora
