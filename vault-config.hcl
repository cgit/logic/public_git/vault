backend "file" {
    path = "/var/lib/vault"
}

listener "tcp" {
    # This is the default address and port that Vault will listen on; if you
    # make changes to this (or enable TLS), be sure to update VAULT_ADDR in
    # /etc/sysconfig/vault as well.
    address = "127.0.0.1:8200"
    tls_disable = 1
}
